package spring_app2;

// Coffee라는 인터페이스를 구현한 클래스. 통상 이렇게 이름을 잘 지음
public class CoffeeImple implements Coffee {
	String type;
	@Override
	public void drink(String name) {
		System.out.println(name + "님이 " + type + "을/를 홀짝홀짝 마셔요");
	}
	
	public String getType() {
		return type;
	}
	// app.xml 파일에서 type의 값을 정해주기 위해서는 setter가 꼭 있어야 한다.
	public void setType(String type) {
		this.type = type;
	}
}
