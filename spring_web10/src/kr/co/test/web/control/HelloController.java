package kr.co.test.web.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	// hello.do ==> hello.jsp를 찾아갈 수 있게
	// hi()를 구성해보세요
	@RequestMapping("/hello.do")
	public String hi(){
		return "hello";
	}
}
