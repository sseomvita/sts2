package kr.co.test.web.control;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

// 전달하고 있는 파일이 올바른 파일인지 검사
// 이건 bean이야 라고 알려주고 싶어
@Service("fileValidator")
public class FileValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		// 검사대상 : target
		// 이때 발생하는 에러 : errors
		UploadFile file = (UploadFile) target;
		
		MultipartFile mf = file.getFile();
		
		// 파일의 사이즈 0 안됨
		if(mf.getSize() == 0){
			errors.rejectValue("file", null, "파일을 선택해주세요");
		}else if(mf.getSize() > 10485760){ // 너무 크면 안됨, 파일사이즈가 10메가 이상이면
			errors.rejectValue("file", null, "10M 이하의 파일만 전송가능합니다");
		}
		
		
	}

}
