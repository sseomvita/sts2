package d20160919;

public class TestMain {
	public static void main(String[] args) {
		String[] strArray = {"Hello" , "World", "H1o", "H2o"};
		
		// Hxo 라는 문자열을 가지고 있다면 화면에 출력
		
		/*for (int i = 0; i < strArray.length; i++) {
			if(strArray[i].length() == 3){ // 글자 수가 3개인 애들만 검사
				char[] cArray = strArray[i].toCharArray();
				if(cArray[0] == 'H'){
					if(cArray[2] == 'o'){
						System.out.println(strArray[i]);
					}
				}
			} // if end
		} // for end
*/		
		
		
		/*String str = "H1o";
	
		for(String x : strArray){
			if(x.charAt(0)=='H' && x.length()==3 && x.charAt(2)=='o'){
				System.out.println(x);
			} // if end
		} // for end
*/		
		
		String str = "H1o"; 
		String p = "^H.o$"; // 패턴 (형식) // 정규표현식
		
		// 정규표현식
		// 특정한 규칙을 가진 문자열의 집합을 표현하는데 사용하는 형식 언어
		

		
		for(String x : strArray){
			// 약속되어있는 패턴과 일치하면 출력해줘
			if(x.matches(p)) System.out.println(x);
		}
	}
}
