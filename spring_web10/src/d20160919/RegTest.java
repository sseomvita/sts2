package d20160919;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegTest {

	public static void main(String[] args) {
		String searchData = "Hon1gGilDong 20 남원_한양 hong@gmail.com 02-1234-5678"
				+ "KoSeong 20 서울 dagda@hanafos.com 010-222-3333"
				+ "이순신 20 부산 lee@gmail.com 080 3333 4444";
		
		// 특정 문자 가져오기
		// Ko라는 글자를 찾고 싶다!
		// Pattern patern = Pattern.compile("Ko");
		// Matcher matcher = patern.matcher(searchData);

		// 문자열에서 숫자만 출력하기
		//Pattern patern = Pattern.compile("\\d");
		//Matcher matcher = patern.matcher(searchData);
		
		// _포함한 문자 검색 (한글 제외)      A~Z, a~z, _
		// Pattern patern = Pattern.compile("\\w");
		// Matcher matcher = patern.matcher(searchData);
		
		// *  : 0번 이상 반복 : 0, 1, n
		// +  : 1번 이상 반복 : 1, n
		// ?  : 0 or 1
		// {} : 횟수를 표시
		
		// 여러자리 숫자만 찾아오기
		// Pattern patern = Pattern.compile("\\d+");
		// Matcher matcher = patern.matcher(searchData);
		
		// 3자리 숫자만 가져오기
		// Pattern patern = Pattern.compile("\\d{3}");
		// Matcher matcher = patern.matcher(searchData);
		// 결과 : 123567010222333080333444
		
		// ^ : 시작문자
		// $ : 종료문자
		// [] : 문자열셋[a-z] : a부터 z사이의 문자라는 의미
		// [^] : 지정한 패턴을 제외한 문자 : [^a-z] : a부터 z사이의 문자가 아닌것
			// []안에 ^를 사용하면 not의 의미를 가진다
		// [\d] : 숫자, [0-9] 와 같은 의미
		// [\D] : 숫자가 아닌 값[^0-9] 와 같은 의미
		// . : 뉴라인(\n) 제외한 한 문자  ex ^H.o$  Hxo인 문자
		// - : 범위지정 메타 문자 : [a-z]
		// \w : 영문자, _ , 숫자 포함되어 있는 영문
		// \W : 영문자, _ , 숫자가 아닌 문자
	    // \s : 공백
	    // \S : 공백을 제외한 문자
		// | : or의 의미를 가진다
		// \\p{Alpha} : 대소문자 알파벳
		// \\p{Digit} : 숫자
		// [가-힣] : 한글
		
		
		// 
		// Pattern patern = Pattern.compile("^[a-z]+");
		// Matcher matcher = patern.matcher(searchData);
		// 결과 : hong
		
		// 첫글자가 대문자 A부터 Z  사이의 글자로 시작
		// + 이런 글자가 한번 이상 반복되며
		// 숫자가 오는 패턴
		// 문자열 앞에를 HONGGILDONG2 이렇게 바꾸면
		// Pattern patern = Pattern.compile("^[A-Z]+[0-9]");
		// Matcher matcher = patern.matcher(searchData);
		// 결과 : HONGGILDONG2 이거 출력됨
		
		// Hon4gGilDong 이거로 바꾸면
		// Pattern patern = Pattern.compile("^[A-Z]+[a-z]+[0-9]");
		// Matcher matcher = patern.matcher(searchData);
		// 결과 : Hon4출력됨
		
		// 대문자H로 시작하고 중간에 2개의 글자가 들어가고 소문자o로 끝나는
		// String searchData2 ="ABC Hello Halo Hipo";
		// Pattern patern = Pattern.compile("H..o");
		// Matcher matcher = patern.matcher(searchData2);
		
		// $는 원래 끝문자를 의미하는데..
		// $5,000.50 을 찾아보자
		// String searchData3 = "zzz $5,000.50 $58";
		// Pattern patern = Pattern.compile("\\$[0-9,.]+");
		// Matcher matcher = patern.matcher(searchData3);
		// 결과 :  $5,000.50 $58
		
		// 한글 출력
		// Pattern patern = Pattern.compile("[가-힣]+");
		// Matcher matcher = patern.matcher(searchData);
		// 결과 : 한양 서울 이순신 부산
		
		// 이메일 유효성검사
		// 중간에 @ 기호가 와야 하면 대소문자, 숫자, _, - 사용가능
		// [a-zA-Z0-9_\\-\\.]+@[a-zA-Z0-9_\\-]
		// 문자로 시작해서 @문자.문자 형식으로 되어있습니다.
		// [a-zA-Z0-9_\\-\\.]+@[a-zA-Z0-9_\\-]+\\.
		// 시작문자는 대소문자, 숫자,_,-,. 사용가능하며 2자이상이다
		// [a-zA-Z0-9_\\-\\.]+@[a-zA-Z0-9_\\-]+\\.[a-zA-Z0-9]{2,}
		Pattern patern = Pattern.compile("[a-zA-Z0-9_\\-\\.]+@[a-zA-Z0-9_\\-]+\\.[a-zA-Z0-9]{2,}");
		Matcher matcher = patern.matcher(searchData);
		
		// Ko라는 글자가 있다면 출력
		while(matcher.find()){
			System.out.println(matcher.group());
		} // while end
	} // main end
	

}
