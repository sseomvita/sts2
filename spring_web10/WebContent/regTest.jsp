<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Document</title>
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("input[type='button']").on("click", function(){
			var email = $("#email").val();
			var pattern = /^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-]+\.[a-zA-Z0-9]{2,}/;
			/* 
				/패턴/ : 패턴 하나만 찾음
				/패턴/g : 패턴 여러개 찾는다
			*/
			var result = email.match(pattern);
			if(!result){
				console.log("올바른 이메일 아님");
				$("#warnEmail").show();
				$("#email").val("").focus();
				$("p").css("color","red");
				return ;
			}
			console.log("올바른이메일");
		})
	});
	
	String.prototype.trim = function(){
		return this.replace(/^\s+|\s+$/g,'');
	};
	function(){
		return this.replace(/^\s+|\s+$/g,'');
	}
	var test = "	aaa 	bbbb 	cccc ";
</script>
</head>
<body>
	email : <input type="email" name="email" id="email" /> <br />
	<div id="warnEmail" style="display:none;">
		<p>잘못된 이메일 형식입니다</p>
	</div>
	<input type="button" value="확인" />
	
</body>
</html>