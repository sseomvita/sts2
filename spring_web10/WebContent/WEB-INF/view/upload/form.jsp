<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Document</title>
</head>
<body>
	<h2>form.jsp파일입니다</h2>     <!-- commandName : uploadFile객체형식으로 보내기 -->
	<form:form method="post" action="upload.do" commandName="uploadFile" enctype="multipart/form-data">
		<input type="file" name="file" id="" />
		
		<!-- 에러가 있으면, span태그로 에러메시지 출력해준다-->
		<!-- path ==> UploadFile의 멤버변수와 이름이 같아야한다.  -->
		<form:errors path="file"  />
		<input type="submit" value="전송" />
	</form:form>
</body>
</html>