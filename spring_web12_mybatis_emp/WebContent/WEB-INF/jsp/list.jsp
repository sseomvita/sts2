<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Document</title>
	<spring:url var="mainCss" value="/resources/css/main.css" />
	<spring:url var="mainJs" value="/resources/js/main.js" />
	
	<script type="text/javascript" src="${mainJs}"> </script>
	<link rel="stylesheet" href="${mainCss}" />
</head>
<body>
	<table>
		<tr>
			<th>사원번호</th>
			<th>사원이름</th>
			<th>직업</th>
			<th>매니저번호</th>
			<th>입사일</th>
			<th>연봉</th>
			<th>수당</th>
			<th>부서번호</th>
		</tr>
		<c:forEach var="vo" items="${list}">
			<tr>
				<td>${vo.empno }</td>
				<td>${vo.ename }</td>
				<td>${vo.job }</td>
				<td>${vo.mgr }</td>
				<td>${vo.hiredate }</td>
				<td>${vo.sal }</td>
				<td>${vo.comm }</td>
				<td>${vo.deptno }</td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="8">
				<input type="button" value="사원추가하기" />
			</td>
		</tr>
	</table>
</body>
</html>