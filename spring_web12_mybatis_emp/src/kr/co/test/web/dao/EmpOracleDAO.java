package kr.co.test.web.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import kr.co.test.web.vo.EmpVO;

public class EmpOracleDAO implements Dao {

	SqlSession ss;
	
	
	public void setSs(SqlSession ss) {
		this.ss = ss;
	}

	@Override
	public List<EmpVO> selectAll() {
		return ss.selectList("kr.co.test.emp.selectAll");
	}

}
