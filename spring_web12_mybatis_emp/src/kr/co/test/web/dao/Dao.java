package kr.co.test.web.dao;

import java.util.List;

import kr.co.test.web.vo.EmpVO;

public interface Dao {
	public List<EmpVO> selectAll();
}
