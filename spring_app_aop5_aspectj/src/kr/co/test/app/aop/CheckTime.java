package kr.co.test.app.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.util.StopWatch;

public class CheckTime {
	
	// 이전 방식은 인터페이스를 구현한 것임
	// 여기서는 매개변수가 중요함!
	public Object logAround(ProceedingJoinPoint pjp) throws Throwable{
		// 메서드 이름 얻어오기
		String methodName = pjp.getSignature().getName();
		System.out.println("현재 실행중인 메서드 : " + methodName);
		
		StopWatch sw = new StopWatch();
		sw.start();
		Object obj = pjp.proceed();
		sw.stop();
		System.out.println("처리시간 : " + sw.getTotalTimeSeconds());
		
		return obj;
	}
}
