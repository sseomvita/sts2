package spring_app1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestMain {
	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Hello h = ac.getBean("bean", Hello.class);
		h.sayHello("아기공룡둘리");
	}
}
