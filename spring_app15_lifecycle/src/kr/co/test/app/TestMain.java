package kr.co.test.app;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain {
	public static void main(String[] args) {
		BeanFactory bf;
		ApplicationContext ac = new ClassPathXmlApplicationContext("app.xml");
		
		Message m = ac.getBean("message", Message.class);
		m.printMsg();
		
		// 스프링 컨테이너 종료  AbstractApplication : ac의 조상격임
		AbstractApplicationContext aac = (AbstractApplicationContext) ac;
		aac.close();
	}
}
