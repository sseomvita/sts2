package kr.co.test.app;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class MessageImple implements Message, InitializingBean, DisposableBean {
	String name;
	
	public MessageImple() {}
	
	public MessageImple(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void printMsg() {
		System.out.println("안녕하세요~ " + name);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// bean이 초기화될때 실행되는 것
		// 초기에 뭔가 하고 싶다면 ? 여기에서 호출하면 되겠지!
		System.out.println("InitializingBean 인터페이스 구현메서드 : 초기화 메서드 정의");
	}

	@Override
	public void destroy() throws Exception {
		// bean이 사라질때 실행되는 것
		// 끝날때 뭔가 하고 싶다면! 여기에서 호출하면 되겠지!
		System.out.println("DisposableBean 인터페이스 구현 메서드 : 종료메서드");
	}

}
