package kr.co.test.app;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class MessageImple2 implements Message{

	String name;
	
	public MessageImple2() {
		System.out.println("기본생성자");
	}
	
	/* InitializingBean, DisposableBean 인터페이스를 구현해도되고,
	 * init() destroy() 직접 정의해서
	 * app.xml에서 속성으로 init-method와 destroy-method 설정해 줄 수 있다
	 * */
	public void init(){
		System.out.println("초기화 메서드 ");
	}
	
	public void destroy(){
		System.out.println("종료 메서드 ");
	}
	
	public void setName(String name) {
		this.name = name;
	}


	@Override
	public void printMsg() {
		System.out.println("메세지 출력 : " + name);
	}

}
