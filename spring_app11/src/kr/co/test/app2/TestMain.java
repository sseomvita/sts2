package kr.co.test.app2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain {
	public static void main(String[] args) {
		// BeanFactory bf;
								// ClassPath는 src 경로
		ApplicationContext ac = new ClassPathXmlApplicationContext("app2.xml");
		SystemMonitor sm = ac.getBean("sm", SystemMonitor.class);
		sm.print();
	}
}
