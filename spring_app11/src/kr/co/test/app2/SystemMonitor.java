package kr.co.test.app2;

import org.springframework.beans.factory.annotation.Autowired;

public class SystemMonitor {
	@Autowired
	/*@Qualifier("") -> 여러개라면 이런식으로, 지정해줄 수 있다*/
	Sender sender; 
	// 상속 : is-a 관계
	// 멤버변수로 가지는 것 : has-a 관계
	public SystemMonitor() {
		// TODO Auto-generated constructor stub
	}
	public SystemMonitor(Sender sender){
		this.sender = sender;
	}
	
	public void print(){
		System.out.println("SystemMonitor 클래스의 print() ");
		sender.show();
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}
	
}
