package spring_app_aop1;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

// 공통 관심사 - 이 코드가 중복되어서 많이 쓰일거야~
// printName과 pringEmail 메서드 수행하기 전에 이 함수를 수행하고 싶다
public class HijackBeforeAdvice implements MethodBeforeAdvice{

	@Override
	public void before(Method method, Object[] args, Object target) throws Throwable {
		System.out.println("============================");
		System.out.println("공통관심사 업무를 지정");
		System.out.println("메서드가 실행되기 전에 가로채기 ");
		System.out.println("============================");
	}
	
}
