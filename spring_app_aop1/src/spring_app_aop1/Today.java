package spring_app_aop1;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.aop.MethodBeforeAdvice;

public class Today implements MethodBeforeAdvice{
	// 오늘날짜 현재 시간을 출력
	@Override
	public void before(Method method, Object[] args, Object target) throws Throwable {
		Date today = new Date();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd E요일 HH:mm:ss");
		
		System.out.println(sdf.format(today));
	}
	
}
