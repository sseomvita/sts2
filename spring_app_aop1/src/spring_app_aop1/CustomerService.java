package spring_app_aop1;

public interface CustomerService {
	void printName();
	void printEmail();
}
