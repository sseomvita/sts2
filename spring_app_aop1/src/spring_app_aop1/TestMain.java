package spring_app_aop1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain {
	public static void main(String[] args) {
		// beanFactory는 aop를 지원하지 않는다
		ApplicationContext ac = new ClassPathXmlApplicationContext("app.xml");
		
		CustomerService cs = ac.getBean("cs", CustomerService.class);
		
		cs.printName();
		cs.printEmail();
	}
}
