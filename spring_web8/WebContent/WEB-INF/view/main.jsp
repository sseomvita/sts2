<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Document</title>
</head>
<body>
	<h3>main 페이지 입니다</h3>
	
	<!-- ne : not equal : != -->
	<c:choose>
		<c:when test="${id ne null }">
			<h3>${id }님 환영합니다</h3>	
		</c:when>
		<c:otherwise>
			<a href="login.do">로그인하러가기</a>
		</c:otherwise>
	</c:choose>
	
</body>
</html>