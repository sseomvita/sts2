package kr.co.test.web.control;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
	
	@RequestMapping("/login.do")
	public String login(){
		return "login/login";
	}

	// 때에 따라, 필요한 매개변수를 넣어주면 된다
	@RequestMapping("/loginOk.do")
	public String logOk(@RequestParam(value="id")String id, 
			@RequestParam(value="pw")String pw, HttpSession hs){
		
		// MeberDAO dao = new MemberDAO();
		
		/*String id = req.getParameter("id");
		String pw = req.getParameter("pw");*/
		
		
		System.out.println("id : " + id);
		System.out.println("pw : " + pw);
		// db연결 후, id pw 검사 후 존재하는 사용자라면 (여기서는 존재하는 아이디라고 가정)
		// 세션에 id 속성 담기 
		hs.setAttribute("id", id);
		return "main";
	}
}
