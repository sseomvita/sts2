package kr.co.test.web.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//@Controller
public class MainController {
	
	@RequestMapping("/main.do")
	public String main(){
		// view의 이름을 main으로 리턴해준다
		return "main";
	}
}
