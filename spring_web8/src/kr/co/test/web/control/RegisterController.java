package kr.co.test.web.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.test.web.dto.MemberDTO;

@Controller
public class RegisterController {
	@RequestMapping("/register/step1.do")
	public String processStep1(){
		return "step1";
	}
	
	@RequestMapping("/register/step2.do")
						// request.getParameter(ck);
						// 아래걸로 하면 형변환도 해준다.
						// 만약 ck의 값을 안 넘겨줬다면, 기본값으로 false가 들어가진다
	public String processStep2(@RequestParam(value="ck", defaultValue="false")Boolean b){
		if(b){ // b가 참으로 넘어왔을 경우
			System.out.println("약관 동의 한 상태");
		}else{
			return "step1"; // 약관 동의하지 않았다면 step1로 넘어가도록!
		}
		return "step2";
	}// processStep2() end
	
	@RequestMapping("/register/step3.do")
				// 처음에 값 가져올 때 부터, dto로 가져오게 해줌
	public ModelAndView processStep3(@ModelAttribute("dto")MemberDTO dto){
		// 파라미터 값 가져오기
		
		// kr.co.test.web.dto.MemberDTO클래스생성
		// 이걸로 dto객체 만들기
		//MemberDTO dto = new MemberDTO(id, pw, confirm, email);
		
		// MemberDAO dao = new MemberDAO();
		// dao.add(dto); 회원가입하려면 이러한 과정이 필요
		// 지금은 했다고 가정하자!!
		
		return new ModelAndView("welcome", "dto", dto);
	}
	
}
