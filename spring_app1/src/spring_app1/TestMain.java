package spring_app1;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain {
	public static void main(String[] args) {
		/*  인터페이스 = 구현한 클래스로 new 해주기!
		 *  -> 해당방법은 main클래스의 변경이 필요하다
		 *  이것도 하지 않도록하는 것이 스프링!
		Hello h = new HelloVN();
		h.sayHello("아기공룡둘리");*/ 
		
		// 객체관리는 스프링을 통해서 유지
		BeanFactory bf = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
		
		// getBean(" ") -> Object로 리턴해주기때문에, 형변환 필요함!
		Object obj1 = bf.getBean("bean");
		Object obj2 = bf.getBean("bean");
		System.out.println("obj1: " + obj1);
		System.out.println("obj2: " + obj2);
		// ==> 같은 참조값을 출력해준다. 스프링은 객체를 알아서, 싱글톤으로 관리해준다.
		Hello h = (Hello)obj1;
		h.sayHello("아기공룡둘리");
	}
}
