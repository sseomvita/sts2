package spring_app1;

public interface Hello {
	// 추상메소드이기때문에, abstract를 써줘야하지만
	// 인터페이스는 추상메소드와 상수만으로 이루어지기때문에
	// 생략해도 된다.
	public void sayHello(String name);
}
