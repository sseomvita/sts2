package kr.co.test.app;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

public class HijackGuGu implements MethodBeforeAdvice{

	@Override
	public void before(Method method, Object[] args, Object target) throws Throwable {
		for (int i = 2; i < 10; i++) {
			for (int j = 1; j < 10; j++) {
				System.out.print( i + "*" + j + "=" + i*j + " ");
			}
			System.out.println();
		}// for end
	}// before end
	
}
