package kr.co.test.app;

public class HelloImple implements Hello {
	String name;
	
	public HelloImple() {
		// TODO Auto-generated constructor stub
	}

	public HelloImple(String name) {
		super();
		this.name = name;
	}

	@Override
	public void sayHello() {
		System.out.println("안녕하세요 ~ " + name + "님");
	}

}
