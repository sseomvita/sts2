package kr.co.test.web.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.test.web.dao.MemberDAO;
import kr.co.test.web.dto.MemberDTO;

@Controller
public class LoginController {
	
	/*@RequestMapping("/login.do")
	public String login(){
		return "loginForm";
	}*/
	
	@RequestMapping("/loginOk.do")
	public ModelAndView loginOk(@ModelAttribute("dto")MemberDTO dto){
		// db연결
		// 로그인 성공
		MemberDAO dao = new MemberDAO();
		System.out.println(dao.selectOne(dto));
		if(dao.selectOne(dto)){
			return new ModelAndView("index","dto",dto);
		}else{
			return new ModelAndView("redirect:/login.do");
		}
	}
}
