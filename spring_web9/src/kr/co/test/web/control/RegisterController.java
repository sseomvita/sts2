package kr.co.test.web.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import kr.co.test.web.dao.MemberDAO;
import kr.co.test.web.dto.MemberDTO;

@Controller
public class RegisterController {
	/*@RequestMapping("/reg/reg.do")
	public String registerForm(){
		return "register/form";
	}*/
	
	@RequestMapping("/reg/registerOk.do")
	public String registerOk(@ModelAttribute("dto")MemberDTO dto){
		// DB연결
		MemberDAO dao = new MemberDAO();
		dao.insertOne(dto);
		
		/* 페이지 이동 방식
		 * forward방식 - 주소창은 바뀌지 않고 페이지만 바뀌는 것
		 * redirect방식 - 주소창이 바뀌면서 페이지 이동
		 * */
		
		// 스프링에서 redirect하는 방법.
		// view를 거치지 않고 바로 옮겨가기
		return "redirect:/login.do";
	}
}
