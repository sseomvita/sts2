package spring_app_aop3;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.aop.AfterReturningAdvice;

public class LoggingAdvice implements AfterReturningAdvice{

	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		//System.out.println("이후 출력 test");
		StringBuffer txt = new StringBuffer();
		
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		txt.append(sdf.format(today));
		txt.append(" : 비밀리에 이체 진행중입니다.");
		
		
		File f = new File("C:\\log\\"+sdf.format(today)+".txt");
		FileWriter fw = new FileWriter(f);
		fw.write(txt.toString());
		fw.flush();
		fw.close();
	}

}
