package kr.co.test.app;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NowTime {
	public String getTime(){
		/*StringBuffer sb = new StringBuffer();
		Calendar cal = Calendar.getInstance();
		sb.append(cal.get(cal.HOUR));
		sb.append("시 ");
		sb.append(cal.get(cal.MINUTE));
		sb.append("분");
		return sb.toString(); // 10시 05분 이런 형태로 리턴하고 싶다.
*/	
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(" HH 시 mm 분");
		
		return sdf.format(d);
	}
}
