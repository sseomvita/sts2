package kr.co.test.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class GreetingImple implements Greeting {
	
	// 스프링! 니가 찾아서 넣어줘!
	// xml에서 의존성의 주입(DI)을 따로 하지 않아도 자동으로 해주도록!
	// Autowired를 쓰려면, xml에서 Autowired...processor 써줘야한다.
	// type : 자료형으로 검색 => xml의 id는 의미없다.
	// 인터페이스라면 인터페이스를 구현한 클래스를 찾는다.
	@Autowired
	@Qualifier("nt1") // 이름 써주기
	NowTime nt;
	String msg;
	
	public GreetingImple() {
	}
	
	public GreetingImple(NowTime nt, String msg) {
		this.nt = nt;
		this.msg = msg;
	}
	
	public void setNt(NowTime nt) {
		this.nt = nt;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public void pringMsg() {
		System.out.println("지금 시간은 " + nt.getTime() + "\n전달 메시지 : " + msg);
	}

}
