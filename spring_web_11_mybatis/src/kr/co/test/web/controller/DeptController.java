package kr.co.test.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.co.test.web.dao.Dao;
import kr.co.test.web.dto.DeptDTO;

@Controller
public class DeptController {
	@Autowired
	Dao dao;
	
	@RequestMapping("/deptList.do")
	public ModelAndView list(){
		return new ModelAndView("list", "list", dao.selectAll());
	}
	
	@RequestMapping(value="/add.do", method=RequestMethod.GET)
	public String insertForm(){
		// 따로 실행하는 내용이 없으므로, spring-ctx에서 설정해줘도됨
		return "insertForm";
	}
	
	
	// add.do post방식일 때 호출
	@RequestMapping(value="/add.do", method=RequestMethod.POST)
	public String insertOk(@ModelAttribute("deptDto")DeptDTO dto){
		dao.add(dto);
		// 완료 후 list 페이지로 이동
		return "redirect:/deptList.do";
	}
}
