<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8" />
<title>Document</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

<script src="//cdn.ckeditor.com/4.5.10/basic/ckeditor.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {
		CKEDITOR.replace('editor1');
	})
</script>
<style>
	#container{
		width: 400px;
		margin: auto;
		margin-top: 100px;
	}
</style>
</head>
<body>
	<div id="container">
		<form action="modifyOk">
			<table class="table">
				<tr>
					<th>no</th>
					<td>${dto.no}<input type="hidden" name="no" value="${dto.no}"/> </td>
				</tr>
				<tr>
					<th>제목</th>
					<td><input type="text" name="title" id="" value="${dto.title }" /></td>
				</tr>
				<tr>
					<td colspan="2"><textarea name="contents" id="editor1"
							cols="30" rows="10">${dto.contents}</textarea></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="수정완료" />
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>