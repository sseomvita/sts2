<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8" />
<title>Document</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
	
<style type="text/css">
	#container{
		width: 500px;
		margin: auto;
	}
</style>
</head>
<body>
	<div id="container">
		<table class="table">
			<tr>
				<th>제목</th>
				<td>${bdto.title }</td>
			</tr>
			<tr>
				<th>작성자</th>
				<td>${bdto.writer}</td>
			</tr>
			<tr>
				<td colspan="2">${bdto.contents}</td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="list" class="btn btn-success">목록</a>
					<a href="modify?no=${bdto.no}" class="btn btn-primary">수정</a>
					<a href="delete?no=${bdto.no}" class="btn btn-danger">삭제</a>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>