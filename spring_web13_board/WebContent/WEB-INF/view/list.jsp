<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8" />
<title>Document</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
<style type="text/css">
#container{
	width: 800px;
	height: 850px;
	margin: 20px auto;
}
</style>
</head>
<body>
	<div id="container">
	총 데이터 수 : ${total} <br />
	시작 번호 ${startNo } <br />
	끝 번호 ${endNo }
	총 페이지 수 : ${totalPage } <br />
	시작페이지번호 : ${startPage } <br />
	끝 페이지 번호 : ${endPage } <br />
	
		<table class="table table-striped">
			<tr class="danger">
				<th>게시물번호</th>
				<th>작성자</th>
				<th>제목</th>
				<th>내용</th>
				<th>조회수</th>
				<th>작성날짜</th>
				<th>상태</th>
			</tr>
			<c:forEach var="dto" items="${list}">
				<tr class="danger">
					<td>${dto.no }</td>
					<td>${dto.writer }</td>
					<td><a href="detail?no=${dto.no}">${dto.title }</a></td>
					<td>${dto.contents }</td>
					<td>${dto.hits }</td>
					<td>${dto.wdate }</td>
					<td><a href="delete?no=${dto.no}" class="btn btn-danger">삭제</a></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="7">
					
						<ul class="pagination">
							<c:if test="${startPage>1}">
								<li><a href="list?currentPage=${startPage-1}" aria-label="Previous"> <span
									aria-hidden="true">&laquo;</span>
								</a></li>
							</c:if>
							
							<c:forEach var="i" begin="${startPage}" end="${endPage}">
								<li><a href="list?currentPage=${i}">${i}</a></li>
							</c:forEach>


							<c:if test="${endPage<totalPage}">
								<li><a href="list?currentPage=${startPage+1}" aria-label="Next"> <span
									aria-hidden="true">&raquo;</span>
								</a></li>
							</c:if>
							
						</ul>
					
				</td>
			</tr>
			<tr class="danger">
				<td colspan="7"><a href="writeForm" class="btn btn-warning">글쓰기</a>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>