package kr.co.test.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.test.web.dao.Dao;
import kr.co.test.web.dto.BoardDTO;

@Controller
public class BoardController {
	
	// Dao interface를 구현한 애를 자동으로 넣어준다.
	@Autowired // autowired가 되려면, 스프링이 BoardOracleDao를 알고 있어야됨. bean등록해주기
	Dao dao;
	
	@RequestMapping("/list")
	public ModelAndView boardList(@RequestParam(value="currentPage", defaultValue="1")int currentPage,
								Model model){
		// 총 데이터의 갯수 ?
		int total = dao.getTotal();
		// 현재 페이지의 시작 번호 ?
		int startNo = (currentPage-1)*10+1;
		// 현재 페이지의 끝 번호?
		int endNo = (currentPage)*10;
		// 총 페이지 번호?
		int totalPage = total/10;
		// 시작 페이지 번호 ?
		int startPage = currentPage-5<=0?1:currentPage-5;
		// 끝 페이지 번호?
		int endPage = startPage+10 >= totalPage ? totalPage : startPage+10;
		
		model.addAttribute("total", total);
		model.addAttribute("startNo", startNo);
		model.addAttribute("endNo", endNo);
		model.addAttribute("totalPage", totalPage);
		model.addAttribute("startPage", startPage);
		model.addAttribute("endPage", endPage);
		
		return new ModelAndView("list","list", dao.selectList(startNo, endNo));
	}
	
	@RequestMapping("/writeForm")
	public String form(){
		return "writeForm";
	}
	
	@RequestMapping("/writeOk")
	public String writeOk(@ModelAttribute("dto")BoardDTO dto){
		dao.insertOne(dto);
		return "redirect:/list";
	}
	
	@RequestMapping("/detail")
	public ModelAndView detailPage(@RequestParam(value="no")int no){
		return new ModelAndView("detail", "bdto", dao.selectOne(no));
	}
	
	@RequestMapping("/modify")
	public ModelAndView modifyForm(@RequestParam("no")int no){
		BoardDTO dto = dao.selectOne(no);
		return new ModelAndView("modifyForm","dto",dto);
	} // modifyForm() end
	
	@RequestMapping("/modifyOk")
	public String modifyOk(@ModelAttribute("dto")BoardDTO dto){
		dao.updateOne(dto);
		return "redirect:/list";
	}
	
	@RequestMapping("/delete")
	public String delete(@RequestParam("no")int no){
		dao.deleteOne(no);
		return "redirect:/list";
	}
} // BoardController class end
