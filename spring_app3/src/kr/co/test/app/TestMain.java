package kr.co.test.app;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain {
	public static void main(String[] args) {
		BeanFactory bf = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
		// 원래는 bf.getBean(" ")으로 하면 형변환 해줘야했었다
		// 뒤에 오는 클래스의 타입으로 리턴해준다
		Printer p = bf.getBean("printer", Printer.class);
		p.printing("점심은 뭐 먹지?");
	}
}
