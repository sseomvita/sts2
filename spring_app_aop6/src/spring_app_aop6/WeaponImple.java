package spring_app_aop6;

public class WeaponImple implements Weapon {
	String type;
	
	public WeaponImple() {	}
	
	
	public void setType(String type) {
		this.type = type;
	}


	@Override
	public void fire() {
		System.out.println(type+"으로 발사~");
	}

	@Override
	public void reload() {
		System.out.println("재장전");
	}

}
