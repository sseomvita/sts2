package kr.co.test.app;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionImple implements CollectionInter {

	private List<String> list;
	private Map<String, String> map;
	
	public CollectionImple() {
	}
	
	public CollectionImple(List<String> list, Map<String, String> map) {
		super();
		this.list = list;
		this.map = map;
	}
	
	public void setList(List<String> list) {
		this.list = list;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	@Override
	public void printList() {
		for (String x : list) {
			System.out.println(x);
		}
		System.out.println("=============printList end=============");
	}

	@Override
	public void printMap() {
		/*Set<String> key = map.keySet(); // set계열로 key들을 리턴 -> set은 순서가 없다
		Iterator<String> it = key.iterator();
		while(it.hasNext()){
			String x = it.next();
			System.out.println(x + " : " + map.get(it.next())); // it.next()는 key값을 리턴
		}*/
		
		// 향상된 for문 써서하기!
		// key값을 하나씩 넘겨줌!
		for(String key : map.keySet()){
			System.out.println(key + " : " + map.get(key));
		}
		
		System.out.println("=============printMap end=============");
	}

}
