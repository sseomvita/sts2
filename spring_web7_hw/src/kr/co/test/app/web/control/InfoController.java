package kr.co.test.app.web.control;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InfoController {
	@RequestMapping(value = "info.nhn")
	public String info(Model m, HttpServletRequest req) throws UnknownHostException {
		InetAddress local = InetAddress.getLocalHost();
		m.addAttribute("ip", local.getHostAddress());

		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		int dayNum = cal.get(Calendar.DAY_OF_WEEK);
		String day = "";
		switch (dayNum) {
		case 1:
			day = "일요일";
			break;
		case 2:
			day = "월요일";
			break;
		case 3:
			day = "화요일";
			break;
		case 4:
			day = "수요일";
			break;
		case 5:
			day = "목요일";
			break;
		case 6:
			day = "금요일";
			break;
		case 7:
			day = "토요일";
			break;
		}
		m.addAttribute("day", day);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		m.addAttribute("time", sdf.format(d));

		return "info";
	}
}
