package kr.co.test.app;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.util.StopWatch;

// 1. interface 구현
// 2. AspectJ
// 3. Annotation
@Aspect
public class CheckTime {
	
	@Pointcut("execution (public void printName(..))")
	public void publicTarget(){}
	
	@Around("publicTarget()")
	public Object logAround(ProceedingJoinPoint pjp) throws Throwable{
		
		StopWatch sw = new StopWatch();
		sw.start();
		Object obj = pjp.proceed();
		sw.stop();
		
		System.out.println("동작 시간 : " + sw.getTotalTimeSeconds());
		
		return obj;
	}
}
