package kr.co.test.app;

import java.util.List;

import kr.co.test.app.dao.DeptDAO;
import kr.co.test.app.vo.DeptVO;

public class TestMain {
	public static void main(String[] args) {
		// 클래스가 메모리에 로딩되는 순간
		// static, static method 등록
		// static block 실행
		
		// 객체를 new 하기 전에, msg를 사용할 수 있다.(static 변수이기때문에!)
		System.out.println(StaticConnectionManager.msg);
		StaticConnectionManager.print();
		
		StaticConnectionManager cm = new StaticConnectionManager();
		
		
	}
}
