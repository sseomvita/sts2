<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8" />
<title>Document</title>
</head>
<body>
	<h3>printHobby.jsp파일입니다</h3>
	<ul>
		<c:forEach var="h" items="${hobby}">
			<li>${h }</li>
		</c:forEach>
	</ul>
	<%-- <h2>당신의 취미는 ?</h2>
	<ul>
		<li>${hobby[0] }</li>
		<li>${hobby[1] }</li>
		<li>${hobby[2] }</li>
		<li>${hobby[3] }</li>
		<li>${hobby[4] }</li>
	</ul>	 --%>
</body>
</html>