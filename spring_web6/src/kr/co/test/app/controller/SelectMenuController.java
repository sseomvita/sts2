package kr.co.test.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SelectMenuController {
	
	// Controller를 상속받는 게 아니라, Controller annotation을 사용하면,
	// 하나의 Controller에서 여러개의 메소드를 가질 수 있다
	
	// 사용자가 selectHobby.do 이렇게 요청하면 이 메소드를 수행해줘.
	@RequestMapping(value="selectHobby.do")
	public ModelAndView ccc(HttpServletRequest req){
		String[] hobby = req.getParameterValues("hobby");
		ModelAndView mav = new ModelAndView("printHobby", "hobby", hobby);
			// new ModelAndView(뷰이름, 모델객체명, 전달객체)
		return mav;
	}
	
	/*@RequestMapping(value="lunch.do")
	public ModelAndView bbb(){
		return new ModelAndView("goodTime", "msg", "Enjoy your lunch!");
	}*/
	
	// 이렇게, String 타입으로 리턴해도 된다!! 리턴하는 String은 view의 이름 (가야하는 jsp파일)
	@RequestMapping(value="lunch.do")
	public String bbb(Model m){
		m.addAttribute("msg", "Enjoy your Lunch!!"); 
		return "goodTime";
	}
	
	@RequestMapping(value="goHome.do")
	public ModelAndView aaa(){
		return new ModelAndView("gohome","msg","집에가고싶어!!");
	}
	
	 // ==> 매개변수에 
	 // HttpServletRequest, HttpServletResponse, Model, HttpSession(session유지하고 싶을 때! - ex)로그인 )
}
