package kr.co.test.app;
// app.xml 역할을 하는 객체

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration  // 이건 설정을 하는 애야!
public class AppConfig {
	
	// 설정역할하는애는 메소드가 꼭 필요하다
	@Bean(name="helloBean") // <bean id="helloBean" class="HelloImple"></bean>
	public Hello helloWorld(){
		return new HelloImple();
	}
}
