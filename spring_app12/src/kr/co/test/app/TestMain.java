package kr.co.test.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestMain {
	public static void main(String[] args) {
/*		ApplicationContext ac = new ClassPathXmlApplicationContext("app.xml");*/
		
		
		ApplicationContext ac = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Hello h = ac.getBean("helloBean", Hello.class);
		h.printHello("Spring Java Config");
		
	}
}
