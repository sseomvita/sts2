package kr.co.test.app;

public interface Character {
	public void walk();
	public void eat(String it); // 물약먹기
	public void attack(Object obj);
	public void get(Object obj); // 아이템 줍기
}
