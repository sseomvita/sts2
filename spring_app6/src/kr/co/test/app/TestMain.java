package kr.co.test.app;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain {
	public static void main(String[] args) {
		BeanFactory bf = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
		
		Data d = bf.getBean("data", Data.class);
		/* Data 클래스에는 int number 멤버변수
		 * setter, getter 구성되어있다
		 * */
		
		// d.setNumber(300); -> 이걸 안 하고도 값을 넣고싶다 -> 스프링 설정에서 property태그 이용
		System.out.println("출력 값 : " + d.getNumber());
	}
}
