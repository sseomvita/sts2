package kr.co.test.app2;

public class SystemMonitor {
	Sender sender; 
	// 상속 : is-a 관계
	// 멤버변수로 가지는 것 : has-a 관계
	public SystemMonitor() {
		// TODO Auto-generated constructor stub
	}
	public SystemMonitor(Sender sender){
		this.sender = sender;
	}
	
	public void print(){
		System.out.println("SystemMonitor 클래스의 print() ");
		sender.show();
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}
	
}
