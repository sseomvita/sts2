package kr.co.test.app3;

public class LaserPrinter implements Printer {
	int red, green, blue; // 카트리지 잔량
	// 매개변수 있는 생성자
	// 생성자
	
	public LaserPrinter() {}
	
	public LaserPrinter(int red, int green, int blue) {
		super();
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
	
	public void setRed(int red) {
		this.red = red;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}
	
	@Override
	public void print() {
		// r, g, b가 0이상일때만 컬러로 출력합니다.
		// 카트리지를 갈아주세요. 메세지 출력
		if( red > 0 && green > 0 && blue > 0){
			System.out.println("출력합니다~");
			/*red -= 50;
			green -= 50;
			blue -= 50;*/
			
			System.out.println("red 잔량 : " + red);
			System.out.println("green 잔량 : " + green);
			System.out.println("blue 잔량 : " + blue);
		}else{
			System.out.println("카트리지를 갈아주세요~");
		}
	} // print() end
}
