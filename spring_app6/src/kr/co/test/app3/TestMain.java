package kr.co.test.app3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain {
	public static void main(String[] args) {
		/* BeanFactory bf;
		 * bean 정의 파일 읽기
		 * =====================
		 * 메세지 소스, 이벤트 처리
		 * AOP
		 * 다국어처리 (사이트 들어갔을 때, 언어 선택하는 것, 한국어 영어 중국어 ..등)
		 * ApplicationContext 
		 * 
		 *   => BeanFactory는 선 위의 기능만 지원함
		 *   아래는 ApplicationContext는 BeanFactory기능에 아래 기능 추가됨
		 * */
		
		ApplicationContext ac = new ClassPathXmlApplicationContext("app3.xml");
		Printer p = ac.getBean("printer", Printer.class);
		
		p.print();
	}
}
