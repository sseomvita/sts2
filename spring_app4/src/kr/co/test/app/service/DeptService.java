package kr.co.test.app.service;

import kr.co.test.app.dao.CommonDAO;
import kr.co.test.app.vo.DeptVO;

public class DeptService implements DBService{

	CommonDAO dao;

	// 아직은 무슨 DB를 쓸 지 몰라, 생성자 호출할 때 매개변수로 넘겨주면서
	// 어떤 DB를 사용할지 결정해준다.
	public DeptService(CommonDAO dao) {
		this.dao = dao;
		this.dao.connect();
	}
	
	@Override
	public void readAll() {
		dao.selectAll();
		
	}

	@Override
	public void readOne(int no) {
		dao.selectOne(no);
	}

	@Override
	public void write(DeptVO vo) {
		dao.insert(vo);
	}

	@Override
	public void modify(DeptVO vo) {
		dao.update(vo);
	}

	@Override
	public void drop(int no) {
		dao.delete(no);
	}

}
