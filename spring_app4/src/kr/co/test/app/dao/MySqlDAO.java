package kr.co.test.app.dao;

import java.util.ArrayList;

import kr.co.test.app.vo.DeptVO;

public class MySqlDAO implements CommonDAO {

	@Override
	public void connect() {
		System.out.println("Mysql DAO연결");
	}

	@Override
	public void insert(DeptVO vo) {
		System.out.println("++dept 데이터 1건 입력++");

	}

	@Override
	public void update(DeptVO vo) {
		System.out.println("++데이터 업데이트++");
	}

	@Override
	public void delete(int no) {
		System.out.println("delete 실행됨");
	}

	@Override
	public DeptVO selectOne(int no) {
		System.out.println("데이터 한권 조회");
		return null;
	}

	@Override
	public ArrayList<DeptVO> selectAll() {
		System.out.println("전체조회");
		return null;
	}

	@Override
	public void close() {
		System.out.println("자원반납");
	}

}
