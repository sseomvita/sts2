package spring_app_aop2;


public interface CustomerService {
	void printName();
	void printEmail();
}
