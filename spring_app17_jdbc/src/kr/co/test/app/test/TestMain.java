package kr.co.test.app.test;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import kr.co.test.app.dao.Dao;
import kr.co.test.app.vo.DeptVO;

public class TestMain {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ApplicationContext ac = new ClassPathXmlApplicationContext("app.xml");
		Dao dao = ac.getBean("dao", Dao.class);
		
		int deptno = 0;
		String dname = "";
		String loc = "";
		
		// ********************  select All ******************** //
		List<DeptVO> list = dao.selectAll();
		
		for(DeptVO vo : list){
			System.out.println(vo.getDeptno() + " : " + vo.getDname() + " : " + vo.getLoc());
		} // for end
		
		/*// ********************  insert One ******************** //
		System.out.println("입력할 부서번호 : ");
		int deptno = sc.nextInt();
		System.out.println("입력할 부서 이름 : ");
		String dname = sc.next();
		System.out.println("입력할 부서의 지역 : ");
		String loc = sc.next();
		DeptVO vo = new DeptVO(deptno, dname, loc);
		dao.insertOne(vo);*/
		
		
		
		// ********************  update ******************** //
		System.out.println("수정할 부서의 번호 : ");
		deptno = sc.nextInt();
		System.out.println("수정할 부서의 이름 : ");
		dname = sc.next();
		System.out.println("수정할 부서의 위치 : ");
		loc = sc.next();
		
		DeptVO vo2 = new DeptVO(deptno, dname, loc);
		dao.updateOne(vo2);
		
		// ********************  select One ******************** //
		System.out.println("검색할 부서의 번호를 입력하세요! (방금 수정한 부서 확인하기)");
		deptno = sc.nextInt();
		DeptVO vo3 = dao.selectOne(deptno);
		System.out.println("수정한 데이터 : " + vo3.getDeptno() + " : " + vo3.getDname() + " : " 
							+ vo3.getLoc());
		
		// ********************  delete One ******************** //
		System.out.println("삭제할 부서의 번호를 입력해주세요");
		deptno = sc.nextInt();
		dao.deleteOne(deptno);
		
		// ********************  select All ******************** //
		System.out.println("==============  데이터 삭제 check  ================");
		list = dao.selectAll();
		
		for(DeptVO x : list){
			System.out.println(x.getDeptno() + " : " + x.getDname() + " : " + x.getLoc());
		} // for end
		
	}
}
