package kr.co.test.app;

import java.util.Calendar;

public class NowTime {
	Calendar cal;
	public NowTime() {
		this.cal = Calendar.getInstance();
	}
	public int getHour(){
		// 지금 시간 알아와서 리턴.
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		
		// cal.get(Calendar.HOUR); -> 12시간 기준
		// cal.get(Calendar.HOUR_OF_DAY); -> 24시간 기준
		
		return hour;
	}
}
