package kr.co.test.app;

public class HelloImple implements Hello {
	NowTime nt;
	String msg;
	
	public HelloImple(NowTime nt){
		this.nt = nt;
	}
	
	@Override
	public void sayHello() {
		int hour = nt.getHour();
		
		// 시간에 따라 msg의 값을 정하기
		/*오전 : Good Morning~
		오후 : 점심먹었니?
		저녁 : 수고했어 오늘도~*/
		if( hour >= 5 && hour <10){
			msg = "Good Morning";
		}else if( hour > 10 && hour < 17){
			msg = "점심먹었니?";
		}else{
			msg = "수고했어 오늘도~ ^^";
		}
		System.out.println(msg);
	} // sayHello() end

}
