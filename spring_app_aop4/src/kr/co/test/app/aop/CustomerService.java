package kr.co.test.app.aop;


public interface CustomerService {
	void printName();
	void printEmail();
}
