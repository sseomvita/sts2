package kr.co.test.app;

public class MonsterBall implements Weapon {
	int count;
	
	public MonsterBall() {
		count = 5;
	}
	
	public void pickUp() {
		// 몬스터볼 주음
		count++;
	}
	
	public void throwBall(){
		if(count > 0){
			System.out.println("나가라 몬스터 볼!");
			count--;
		}else{
			System.out.println("몬스터 볼 다 사용했음~"
					+ "몬스터볼을 주워주세요");
		}
	}
	
	@Override
	public void use() {
		throwBall();
	}

	@Override
	public void drop() {
		System.out.println("포켓몬 잘 가 ~");
	}

	@Override
	public void reuse() {
		pickUp(); 
	}

}
