package kr.co.test.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

public class Police implements Character {
	// 무기를 상속받으면, 무기를 버릴 수 없다.
	// 무기를 가지기 위해서는 상속이 아닌, 멤버 변수로 갖기!
	@Autowired
	@Qualifier("mb")
	Weapon w;
	@Value("100")
	int hp;
	public Police() {}
	// 생성자를 통해서 참조값 전달
	public Police(Weapon w, int hp) {
		super();
		this.w = w;
		this.hp = hp;
	}
	
	@Override
	public void walk() {
		System.out.println("경찰이 뚜벅뚜벅 걸어요");
	}

	@Override
	public void eat(String it) {
		System.out.println("경찰이 " + it + "를 냠냠냠");
	}

	@Override
	public void attack(Object obj) {
		w.use(); // 무기를 사용해서 공격하기
		System.out.println( obj + "을/를 공격합니다");
	}

	@Override
	public void get(Object obj) {
		System.out.println(obj + "을/를 획득합니다");
	}
	@Override
	public void status() {
		System.out.println("나의 체력 : " + hp);
	}

}
